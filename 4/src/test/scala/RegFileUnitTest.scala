import java.io.File
import chisel3._   //needed to declare testbench (tb) registerFile
import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}
import chisel3.util.experimental.loadMemoryFromFile



class LoadMemoryFromFileTester(c: ViewRegFile, val regnum: Int, val size: Int) extends PeekPokeTester(c) {
  for(addr <- 0 until regnum) {
    println("Entering for loop")
    poke(c.io.registers(0),BigInt(1434))
    poke(c.io.registers(1),BigInt(9082))
    step(1)
    poke(c.io.inputs, addr) //desired values from function registerFile
    poke(c.io.ok,1)
    step(1)
    poke(c.io.ok,0) 
    println(f"peek from $addr ${peek(c.io.values)}%x ")
  }
}

// class RegUnitTester(c: ViewRegFile, val regnum: Int, val size:Int) extends PeekPokeTester(c) {
//   private val mod = c
//   val register = Array.fill(regnum)(BigInt(0)) // Using BigInt because Int is 32-bit
//   register(1) = 1434 // I didn't wrap in BigInt because Scala will convert from Int automatically
//   register(2) = 9082 // If you create values that don't fit in 32-bit *signed* Int, wrap in BigInt(...)
//   // You can't poke internal values, there is some support for this, see:
//   // https://github.com/freechipsproject/chisel3/wiki/Chisel-Memories#loading-memories-in-simulation
//   //poke(mod.io.registers, register) //loaded tb registerFile to function registerFile
//   val low = Module(new LoadMemoryFromFileTester(c,regnum, size))
//   // for(j<- 0 until regnum) {
//   //   poke(mod.io.inputs, j) //desired values from function registerFile
//   //   poke(mod.io.ok,1)
//   //   step(1)
//   //   poke(mod.io.ok,0)
//   //   peek(mod.io.values) // indexed output
//   // }
// }

class EditMemoryFromFileTester(c: EditRegFile, val regnum: Int, val size: Int) extends PeekPokeTester(c) {
  val mem = Array.fill(regnum)(BigInt(0)) 
  for(i <-0 until regnum) {
    val j : Int = i * 2
    mem(i) = j
  }
  poke(c.io.registers,mem)
  for(addr <- 0 until regnum) { 
    // poke(c.io.registers(0),BigInt(1434))
    // poke(c.io.registers(1),BigInt(9082))
    // step(1)
    println(f"peek before $addr ${peek(c.io.registers(addr))}%x ")
    poke(c.io.inputs, addr) //desired values from function registerFile
    poke(c.io.ok,1)
    step(1)
    poke(c.io.ok,0) 
    step(1)
    println(f"peek during $addr ${peek(c.io.values(addr))}%x ")
    poke(c.io.inputs, addr) //desired values from function registerFile
    poke(c.io.ok,1)
    step(1)
    poke(c.io.ok,0) 
    println(f"peek after $addr ${peek(c.io.values(addr))}%x ")
    step(1)
  }
}

object RegViewer extends App {
  println("Testing RegFile Viewer")
  iotesters.Driver.execute(Array("--target-dir", "generated", "--generate-vcd-output", "on"), () => new ViewRegFile(4, 16)) {
    c => new LoadMemoryFromFileTester(c,4,16)
  }
}

object RegEditor extends App {   // This test has a half clock cycle differencce between io_ok (lead) and io_inputs. Can't step by hallf 
  println("Testing RegFile Editor")
  iotesters.Driver.execute(Array("--target-dir", "generated", "--generate-vcd-output", "on"), () => new EditRegFile(4, 16)) {
    c => new EditMemoryFromFileTester(c,4,16)
  }
}