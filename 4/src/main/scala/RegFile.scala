// See LICENSE for license details.

// package lalu

import chisel3._
import chisel3.util._

// class RegFileIO(implicit p: Parameters)  extends CoreBundle()(p) {
//   val raddr1 = Input(UInt(5.W))
//   val raddr2 = Input(UInt(5.W))
//   val rdata1 = Output(UInt(xlen.W))
//   val rdata2 = Output(UInt(xlen.W))
//   val wen    = Input(Bool())
//   val waddr  = Input(UInt(5.W))
//   val wdata  = Input(UInt(xlen.W))
// }

// class RegFile(implicit val p: Parameters) extends Module with CoreParams {
//   val io = IO(new RegFileIO)
//   val regs = Mem(32, UInt(xlen.W))
//   io.rdata1 := Mux(io.raddr1.orR, regs(io.raddr1), 0.U)
//   io.rdata2 := Mux(io.raddr2.orR, regs(io.raddr2), 0.U)
//   when(io.wen & io.waddr.orR) {
//     regs(io.waddr) := io.wdata
//   }
// }


class ViewRegFile(val regnum: Int, val size: Int) extends Module {
  val io = IO(new Bundle {
    val registers =  Input(Vec(regnum,UInt(size.W)))
    val inputs = Input(UInt(size.W))
    val ok = Input(UInt(1.W))
    val values = Output(UInt(size.W))
  })
  val buffer = RegInit(VecInit(Seq.fill(regnum)(0.U(size.W)))) // so we have a reset to buffer regmap
  when(io.ok === 1.U){
  for(i<-0 until regnum) {
  buffer(i)  := io.registers(i)
  }
  // io.values := io.registers(io.inputs)
  }
  // for(i<-0 until io.inputs.asInt)
  val itr = io.inputs
  io.values := buffer(itr)
}

class EditRegFile(regnum: Int, size: Int) extends Module{
  val io = IO(new Bundle {
    val registers =  Input(Vec(regnum,UInt(size.W)))
    val inputs = Input(UInt(size.W))
    val ok = Input(Bool())
    val values = Output(Vec(regnum,UInt(size.W)))
  })
  val sNone :: sOne1 :: Nil = Enum(2)
  val state = RegInit(sNone)
  val buffer = RegInit(VecInit(Seq.fill(regnum)(0.U(size.W))))
  val iter = RegInit(0.U(size.W))
  val newval = Reg(UInt(size.W))
  switch (state) {
    is (sNone) {
      when (io.ok) { //intiizlie iterator and register buffer
        // iter := io.inputs.toInt
        iter := io.inputs
        for(i<-0 until regnum) {
        buffer(i)  := io.registers(i)
        }
        state := sOne1
      } .otherwise {
        state := sNone
      }
    }
    is (sOne1) {
      when (io.ok) { // replaced value
        newval := io.inputs
        buffer(iter) := newval
        state := sNone
      } .otherwise {
        state := sOne1
      }
    }
  }
  io.values := buffer

}

class buttonState() extends Module {
  val io = IO(new Bundle {
    val button = Input(UInt(1.W))
    val load = Output(UInt(1.W))
  })
  val off :: on :: Nil = Enum(2)
  val state = RegInit(off)
  val toggle = RegInit(0.U(1.W))
  io.load:=toggle
  switch(state) {
    is(off) {
      when(io.button === 1.U){
        toggle := 1.U
        state := on
      } .otherwise {
        state := off 
      }
    }
    is(on) {
      when(io.button === 1.U) {
        toggle := 0.U
        state := off
      }  .otherwise {
        state := on 
      }
    }
  }
}