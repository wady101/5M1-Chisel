// See README.md for license details.

// package lalu

import chisel3._
import chisel3.util._
import chisel3.experimental._ 

/**
  * Compute GCD using subtraction method.
  * Subtracts the smaller from the larger until register y is zero.
  * value in register x is then the GCD
  */

class seven_seg_ctrl extends BlackBox with HasBlackBoxResource {
  val io = IO(new Bundle() {
    val clk = Input(Clock())
    val number = Input(UInt(16.W))
    val reset = Input(UInt(1.W))
    val LED_out = Output(UInt(7.W))
    val Anode_Activate = Output(UInt(4.W))
  })
  setResource("/seven_seg_ctrl.v")
}
class debouncer extends BlackBox with HasBlackBoxResource {
  val io = IO(new Bundle() {
    val clk = Input(Clock())
    val reset = Input(UInt(1.W))
    val button = Input(UInt(1.W))
    val button_db = Output(UInt(1.W))
  })
  setResource("/debouncer.v")
}

class LALU(regnum: Int, size: Int) extends Module {
  val io = IO(new Bundle {
    val switches        = Input(UInt(size.W))
    val pushbuttons        = Input(UInt(4.W))
    val LCD     = Output(UInt(7.W))
    val Anode_Activate = Output(UInt(4.W))
    // val LED   = Output(UInt(size.W))
  })
  
  val cntReg = RegInit(0.U(size.W))

  /*****
  Config: Seven segment
  *****/
  // val SSS = Vec(Seq.fill(4){Module(new seven_seg_ctrl()).io})
  // for (i <-0 until 4) {
  // SSS(i).reset := reset
  // SSS(i).number := cntReg(i)
  // io.LCD(i) := SSS(i).LED_out
  // io.Anode_activate(i) := SSS(i).Anode_Activate 
  // }
  val SSS = Module(new seven_seg_ctrl())
  SSS.io.clk := clock 
  SSS.io.reset := reset
  SSS.io.number := cntReg
  io.LCD := SSS.io.LED_out
  io.Anode_Activate := SSS.io.Anode_Activate

  /************************
  Config : Debounce 
  Functionality: 
      0
    3 4 1
      2
  ************************/

  val Debounce = Seq.fill(5){Module(new debouncer()).io }
  // val Debounce = Vec(Seq.fill(5){Module(new debouncer()).io }) --> This doesn't work for BlackBoxes :<
  


  val master_switcher = Wire(Vec(4,UInt(1.W)))

  for (i <-0 until 4) {
    Debounce(i).clk := clock 
    Debounce(i).reset := reset
    Debounce(i).button := io.pushbuttons(i)
    master_switcher(i) := Debounce(i).button_db
    }

  val ButtonState = Seq.fill(3){Module(new buttonState()).io }
  ButtonState(0).button := master_switcher(0)
  val load0 = ButtonState(0).load
  ButtonState(1).button := master_switcher(1)
  val load1 = ButtonState(1).load
  ButtonState(2).button := master_switcher(2)
  val load2 = ButtonState(2).load
  
  
  /***********************
  Config: Datapath
  Functionality: 
    0-> load
    1-> store
    2-> opmode
    3-> OK
  ***********************/
  //Instantiate Registers

  // val registerFile  = Reg(Vec(regnum,UInt(size.W)))
  val registerFile = RegInit(VecInit(Seq.fill(regnum)(0.U(size.W))))
  val buffer = Reg(Vec(regnum,UInt(size.W)))
  

  // when((load0 | load2) === 1.U) {
  //   registerFile := buffer 
  // }



  // when(master_switcher(4) === 1.U) {}
  
  when(load0 === 1.U) {
  val ViewRegFile_outer = Module(new ViewRegFile(regnum,size)).io
    // registerFile := buffer   
    ViewRegFile_outer.registers := registerFile
    ViewRegFile_outer.inputs := io.switches
    ViewRegFile_outer.ok := master_switcher(3)
    cntReg := ViewRegFile_outer.values }
  when(load1 === 1.U) {
  val EditRegFile_outer = Module(new EditRegFile(regnum,size)).io
    EditRegFile_outer.registers := registerFile
    EditRegFile_outer.inputs := io.switches
    EditRegFile_outer.ok := master_switcher(3)
    registerFile := EditRegFile_outer.values
  }
  when(load2 === 1.U) {
  val ALU_outer = Module(new ALU(size)).io //Inheritance being used 
    // registerFile := buffer 
    ALU_outer.A := registerFile(0)
    ALU_outer.B := registerFile(1)
    ALU_outer.alu_op := io.switches
    registerFile(0) := ALU_outer.out
  }
  // .else {
  //   // maintain state of LCDs. 
  // }
}

object LALUDriver extends App {
  val regnum = 4
  val width = 16
   chisel3.Driver.execute(args, () => new LALU(regnum,width))
}