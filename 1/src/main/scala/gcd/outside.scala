
import chisel3._
import chisel3.util._
import chisel3.experimental._ 

class outside extends Module {
val io = IO(new Bundle{
val sensor = Input(UInt(2.W))
val clear = Input(Bool())
val decreasectr = Output(UInt(1.W))
})
val off ::a :: ab :: b :: Nil = Enum(4)
val stateReg = RegInit(off)
switch(stateReg) {
is (off) {
when(io.sensor(1.U)){
stateReg:=a} .elsewhen(io.clear){
    stateReg := off
  }
}
is (a) {
when(io.sensor(0.U) & io.sensor(1.U)){
stateReg:=ab} .elsewhen(io.clear) {
stateReg:=a}
}
is (ab) {
when(io.sensor(0.U)){
stateReg:=b} .otherwise {
stateReg:=ab}
}

is (b) {
when(io.clear||io.sensor(0.U) === 0.U){
stateReg:=off} .otherwise {
stateReg:=b}
}
}
io.decreasectr := stateReg === b
}

class insideFSM extends Module {
        val io = IO(new Bundle{
        val sensor = Input(UInt(2.W))
        val clear = Input(Bool())
        val increasectr = Output(UInt(1.W))
        })
  val off::a :: ab :: b :: Nil = Enum(4)
  val stateReg = RegInit(off)
  switch(stateReg) {
  is (off) {
  when(io.sensor(0.U)){
  stateReg:=a  } .elsewhen(io.clear){
    stateReg := off
  }
  }
  is (a) {
    when(io.sensor(0.U) & io.sensor(1.U)){
    stateReg:=ab} .elsewhen(io.clear) {
    stateReg:=a}
  }
  is (ab) {
    when(io.sensor(1.U)){
    stateReg:=b} .otherwise {
    stateReg:=ab}
  }

  is (b) {
    when(io.clear||io.sensor(1.U) === 0.U){
    stateReg:=off} .otherwise {
    stateReg:=b}
  }
  }
  io.increasectr := stateReg === b
  }

class seven_seg_ctrl extends BlackBox with HasBlackBoxResource {
  val io = IO(new Bundle() {
    val number = Input(UInt(16.W))
    val reset = Input(UInt(1.W))
    val LED_out = Output(UInt(7.W))
    val Anode_Activate = Output(UInt(4.W))
  })
  setResource("/seven_seg_ctrl.v")
}

class Execute extends Module {
  val io = IO(new Bundle{
    val enter = Input(UInt(1.W))
    val exit = Input(UInt(1.W))
    // val clear = Input(UInt(1.W))
    val LED_out = Output(UInt(7.W))
    val Anode_activate = Output(UInt(4.W))
  })
  val Inside=Module(new insideFSM())
  val Outside=Module(new outside())
  val SSS = Module(new seven_seg_ctrl())
  val cntReg = RegInit(0.U(8.W))
  val clear = Wire(UInt(1.W))

  
  // Inside.io.sensor(0) := io.enter
  // Inside.io.sensor(1) := io.exit 
  Inside.io.sensor <> Cat(io.exit,io.enter)
  Outside.io.sensor <> Cat(io.exit, io.enter)
  Inside.io.clear := clear
  Outside.io.clear := clear
  // SSS.io.reset := reset
  SSS.io.number := cntReg
  io.LED_out := SSS.io.LED_out
  io.Anode_activate := SSS.io.Anode_Activate 
  
  // Outside.io.sensor(0) :=io.enter
  // Outside.io.sensor(1) := io.exit 
  

  when(Inside.io.increasectr === 1.U) {
    cntReg := cntReg + 1.U
    clear := 1.U
  // } .elsewhen (RegNext(Outside.io.decreasectr === 1.U)) {
  } .elsewhen (Outside.io.decreasectr === 1.U) {
    cntReg := cntReg - 1.U
    clear := 1.U
  } otherwise {
    // cntReg := 0.U
    clear := 0.U
  }

  when(cntReg >= 15.U){
    cntReg := 15.U
  } .elsewhen (cntReg < 0.U) {
    cntReg := 0.U
  }


}
object CarFSMDriver extends App {
  chisel3.Driver.execute(args, () => new Execute)
}
