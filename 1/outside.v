module outside(
  input        clock,
  input        reset,
  input  [1:0] io_sensor,
  input        io_clear,
  output       io_decreasectr
);
  reg [1:0] stateReg; // @[outside.scala 12:23]
  reg [31:0] _RAND_0;
  wire  _T; // @[Conditional.scala 37:30]
  wire  _GEN_9; // @[outside.scala 15:15]
  wire [1:0] _T_1; // @[outside.scala 15:15]
  wire  _T_2; // @[outside.scala 15:15]
  wire  _T_3; // @[Conditional.scala 37:30]
  wire  _T_5; // @[outside.scala 19:15]
  wire  _T_8; // @[outside.scala 19:21]
  wire  _T_11; // @[outside.scala 20:33]
  wire  _T_12; // @[Conditional.scala 37:30]
  wire  _T_15; // @[Conditional.scala 37:30]
  wire  _T_18; // @[outside.scala 30:31]
  wire  _T_19; // @[outside.scala 30:14]
  assign _T = 2'h0 == stateReg; // @[Conditional.scala 37:30]
  assign _GEN_9 = io_sensor[1:1]; // @[outside.scala 15:15]
  assign _T_1 = {{1'd0}, _GEN_9}; // @[outside.scala 15:15]
  assign _T_2 = _T_1[0]; // @[outside.scala 15:15]
  assign _T_3 = 2'h1 == stateReg; // @[Conditional.scala 37:30]
  assign _T_5 = io_sensor[0]; // @[outside.scala 19:15]
  assign _T_8 = _T_5 & _T_2; // @[outside.scala 19:21]
  assign _T_11 = io_clear | _T_5; // @[outside.scala 20:33]
  assign _T_12 = 2'h2 == stateReg; // @[Conditional.scala 37:30]
  assign _T_15 = 2'h3 == stateReg; // @[Conditional.scala 37:30]
  assign _T_18 = _T_5 == 1'h0; // @[outside.scala 30:31]
  assign _T_19 = io_clear | _T_18; // @[outside.scala 30:14]
  assign io_decreasectr = stateReg == 2'h3; // @[outside.scala 35:16]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  stateReg = _RAND_0[1:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      stateReg <= 2'h0;
    end else if (_T) begin
      if (_T_2) begin
        stateReg <= 2'h1;
      end
    end else if (_T_3) begin
      if (_T_8) begin
        stateReg <= 2'h2;
      end else if (_T_11) begin
        stateReg <= 2'h1;
      end
    end else if (_T_12) begin
      if (_T_5) begin
        stateReg <= 2'h3;
      end else begin
        stateReg <= 2'h2;
      end
    end else if (_T_15) begin
      if (_T_19) begin
        stateReg <= 2'h0;
      end else begin
        stateReg <= 2'h3;
      end
    end
  end
endmodule
