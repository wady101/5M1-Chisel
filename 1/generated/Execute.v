module insideFSM(
  input        clock,
  input        reset,
  input  [1:0] io_sensor,
  input        io_clear,
  output       io_increasectr
);
  reg [1:0] stateReg; // @[outside.scala 48:25]
  reg [31:0] _RAND_0;
  wire  _T; // @[Conditional.scala 37:30]
  wire  _T_2; // @[outside.scala 51:17]
  wire  _T_3; // @[Conditional.scala 37:30]
  wire  _GEN_10; // @[outside.scala 57:36]
  wire [1:0] _T_6; // @[outside.scala 57:36]
  wire  _T_7; // @[outside.scala 57:36]
  wire  _T_8; // @[outside.scala 57:25]
  wire  _T_9; // @[Conditional.scala 37:30]
  wire  _T_12; // @[Conditional.scala 37:30]
  wire  _T_15; // @[outside.scala 68:35]
  wire  _T_16; // @[outside.scala 68:18]
  assign _T = 2'h0 == stateReg; // @[Conditional.scala 37:30]
  assign _T_2 = io_sensor[0]; // @[outside.scala 51:17]
  assign _T_3 = 2'h1 == stateReg; // @[Conditional.scala 37:30]
  assign _GEN_10 = io_sensor[1:1]; // @[outside.scala 57:36]
  assign _T_6 = {{1'd0}, _GEN_10}; // @[outside.scala 57:36]
  assign _T_7 = _T_6[0]; // @[outside.scala 57:36]
  assign _T_8 = _T_2 & _T_7; // @[outside.scala 57:25]
  assign _T_9 = 2'h2 == stateReg; // @[Conditional.scala 37:30]
  assign _T_12 = 2'h3 == stateReg; // @[Conditional.scala 37:30]
  assign _T_15 = _T_7 == 1'h0; // @[outside.scala 68:35]
  assign _T_16 = io_clear | _T_15; // @[outside.scala 68:18]
  assign io_increasectr = stateReg == 2'h3; // @[outside.scala 73:18]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  stateReg = _RAND_0[1:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      stateReg <= 2'h0;
    end else if (_T) begin
      if (_T_2) begin
        stateReg <= 2'h1;
      end else if (io_clear) begin
        stateReg <= 2'h0;
      end
    end else if (_T_3) begin
      if (_T_8) begin
        stateReg <= 2'h2;
      end else if (io_clear) begin
        stateReg <= 2'h1;
      end
    end else if (_T_9) begin
      if (_T_7) begin
        stateReg <= 2'h3;
      end else begin
        stateReg <= 2'h2;
      end
    end else if (_T_12) begin
      if (_T_16) begin
        stateReg <= 2'h0;
      end else begin
        stateReg <= 2'h3;
      end
    end
  end
endmodule
module outside(
  input        clock,
  input        reset,
  input  [1:0] io_sensor,
  input        io_clear,
  output       io_decreasectr
);
  reg [1:0] stateReg; // @[outside.scala 13:23]
  reg [31:0] _RAND_0;
  wire  _T; // @[Conditional.scala 37:30]
  wire  _GEN_10; // @[outside.scala 16:15]
  wire [1:0] _T_1; // @[outside.scala 16:15]
  wire  _T_2; // @[outside.scala 16:15]
  wire  _T_3; // @[Conditional.scala 37:30]
  wire  _T_5; // @[outside.scala 22:15]
  wire  _T_8; // @[outside.scala 22:21]
  wire  _T_9; // @[Conditional.scala 37:30]
  wire  _T_12; // @[Conditional.scala 37:30]
  wire  _T_15; // @[outside.scala 33:31]
  wire  _T_16; // @[outside.scala 33:14]
  assign _T = 2'h0 == stateReg; // @[Conditional.scala 37:30]
  assign _GEN_10 = io_sensor[1:1]; // @[outside.scala 16:15]
  assign _T_1 = {{1'd0}, _GEN_10}; // @[outside.scala 16:15]
  assign _T_2 = _T_1[0]; // @[outside.scala 16:15]
  assign _T_3 = 2'h1 == stateReg; // @[Conditional.scala 37:30]
  assign _T_5 = io_sensor[0]; // @[outside.scala 22:15]
  assign _T_8 = _T_5 & _T_2; // @[outside.scala 22:21]
  assign _T_9 = 2'h2 == stateReg; // @[Conditional.scala 37:30]
  assign _T_12 = 2'h3 == stateReg; // @[Conditional.scala 37:30]
  assign _T_15 = _T_5 == 1'h0; // @[outside.scala 33:31]
  assign _T_16 = io_clear | _T_15; // @[outside.scala 33:14]
  assign io_decreasectr = stateReg == 2'h3; // @[outside.scala 38:16]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  stateReg = _RAND_0[1:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      stateReg <= 2'h0;
    end else if (_T) begin
      if (_T_2) begin
        stateReg <= 2'h1;
      end else if (io_clear) begin
        stateReg <= 2'h0;
      end
    end else if (_T_3) begin
      if (_T_8) begin
        stateReg <= 2'h2;
      end else if (io_clear) begin
        stateReg <= 2'h1;
      end
    end else if (_T_9) begin
      if (_T_5) begin
        stateReg <= 2'h3;
      end else begin
        stateReg <= 2'h2;
      end
    end else if (_T_12) begin
      if (_T_16) begin
        stateReg <= 2'h0;
      end else begin
        stateReg <= 2'h3;
      end
    end
  end
endmodule
module Execute(
  input        clock,
  input        reset,
  input        io_enter,
  input        io_exit,
  output [6:0] io_LED_out,
  output [3:0] io_Anode_activate
);
  wire  Inside_clock; // @[outside.scala 94:20]
  wire  Inside_reset; // @[outside.scala 94:20]
  wire [1:0] Inside_io_sensor; // @[outside.scala 94:20]
  wire  Inside_io_clear; // @[outside.scala 94:20]
  wire  Inside_io_increasectr; // @[outside.scala 94:20]
  wire  Outside_clock; // @[outside.scala 95:21]
  wire  Outside_reset; // @[outside.scala 95:21]
  wire [1:0] Outside_io_sensor; // @[outside.scala 95:21]
  wire  Outside_io_clear; // @[outside.scala 95:21]
  wire  Outside_io_decreasectr; // @[outside.scala 95:21]
  wire [15:0] SSS_number; // @[outside.scala 96:19]
  wire  SSS_reset; // @[outside.scala 96:19]
  wire [6:0] SSS_LED_out; // @[outside.scala 96:19]
  wire [3:0] SSS_Anode_Activate; // @[outside.scala 96:19]
  reg [7:0] cntReg; // @[outside.scala 97:23]
  reg [31:0] _RAND_0;
  wire  _T_2; // @[outside.scala 116:30]
  wire [7:0] _T_4; // @[outside.scala 117:22]
  wire  _T_5; // @[outside.scala 120:39]
  wire [7:0] _T_7; // @[outside.scala 121:22]
  wire  _T_8; // @[outside.scala 128:15]
  insideFSM Inside ( // @[outside.scala 94:20]
    .clock(Inside_clock),
    .reset(Inside_reset),
    .io_sensor(Inside_io_sensor),
    .io_clear(Inside_io_clear),
    .io_increasectr(Inside_io_increasectr)
  );
  outside Outside ( // @[outside.scala 95:21]
    .clock(Outside_clock),
    .reset(Outside_reset),
    .io_sensor(Outside_io_sensor),
    .io_clear(Outside_io_clear),
    .io_decreasectr(Outside_io_decreasectr)
  );
  seven_seg_ctrl SSS ( // @[outside.scala 96:19]
    .number(SSS_number),
    .reset(SSS_reset),
    .LED_out(SSS_LED_out),
    .Anode_Activate(SSS_Anode_Activate)
  );
  assign _T_2 = Inside_io_increasectr; // @[outside.scala 116:30]
  assign _T_4 = cntReg + 8'h1; // @[outside.scala 117:22]
  assign _T_5 = Outside_io_decreasectr; // @[outside.scala 120:39]
  assign _T_7 = cntReg - 8'h1; // @[outside.scala 121:22]
  assign _T_8 = cntReg >= 8'hf; // @[outside.scala 128:15]
  assign io_LED_out = SSS_LED_out; // @[outside.scala 109:14]
  assign io_Anode_activate = SSS_Anode_Activate; // @[outside.scala 110:21]
  assign Inside_clock = clock;
  assign Inside_reset = reset;
  assign Inside_io_sensor = {io_exit,io_enter}; // @[outside.scala 103:20]
  assign Inside_io_clear = _T_2 | _T_5; // @[outside.scala 105:19]
  assign Outside_clock = clock;
  assign Outside_reset = reset;
  assign Outside_io_sensor = {io_exit,io_enter}; // @[outside.scala 104:21]
  assign Outside_io_clear = _T_2 | _T_5; // @[outside.scala 106:20]
  assign SSS_number = {{8'd0}, cntReg}; // @[outside.scala 108:17]
  assign SSS_reset = 1'h0;
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  cntReg = _RAND_0[7:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`endif // SYNTHESIS
  always @(posedge clock) begin
    if (reset) begin
      cntReg <= 8'h0;
    end else if (_T_8) begin
      cntReg <= 8'hf;
    end else if (_T_2) begin
      cntReg <= _T_4;
    end else if (_T_5) begin
      cntReg <= _T_7;
    end
  end
endmodule
