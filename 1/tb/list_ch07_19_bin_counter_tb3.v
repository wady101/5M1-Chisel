//Listing 7.19
`timescale 1 ns/10 ps

module bin_counter_tb3();

   //  declaration
   localparam  T=20; // clock period
   wire syn_clr, load, en, up, dec;
   wire [1:0] d;
   wire max_tick, min_tick;
   wire [1:0] q;

    // Copied from generated_tb.v
    wire              clk         ;
    wire              reset         ;
    wire              io_enter      ;
    wire              io_exit       ;
    wire              io_clear      ;
    wire    [7:0]    io_q    ;

    Execute uut (
        .clock         (    clk         ),
        .io_enter      (    d[0]      ),
        .io_exit       (    d[1]       ),
        .reset      (    reset      ),
        .io_q    (    io_q )
    );
    /////

//    parameter PERIOD = 10;
//    initial begin
////        $dumpfile("db_tb_insideFSM.vcd");
////        $dumpvars(0, tb_insideFSM);
//        clk = 1'b0;
//        #(PERIOD/2);
//        forever
//            #(PERIOD/2) clk = ~clk;
//    end


   // uut instantiation
 //  univ_bin_counter #(.N(3)) uut
  //   (.clk(clk), .reset(reset), .syn_clr(syn_clr),
   //   .load(load), .en(en), .up(up), .d(d),
    //  .max_tick(max_tick), .min_tick(min_tick), .q(q));

   // test vector generator
   bin_gen #(.N(2),.T(20)) gen_unit
     (.clk(clk), .reset(reset), .syn_clr(io_clear),
      .load(load), .en(en), .dec(dec), .up(up), .d(d));

   // bin_monitor instantiation
   bin_monitor #(.N(7),.M(8)) mon_unit
     (.clk(clk), .reset(reset), .syn_clr(syn_clr),
      .load(load), .en(en), .dec(dec), .d(d),
      .max_tick(max_tick), .min_tick(min_tick), .q(io_q));

endmodule