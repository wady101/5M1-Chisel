//Listing 7.18
module bin_monitor
   #(parameter N=3,parameter M = 8)
   (
    input wire clk, reset,
    input wire syn_clr, load, en, up,dec,
    input wire [N-1:0] d,
    input wire max_tick, min_tick,
    input wire [M-1:0] q
   );
   reg [N-1:0] q_old, down_old, d_old, gold, en_old, up_old, dec_old;
   reg syn_clr_old, load_old, den_old ;
   reg [39:0] err_msg; // 5-letter message
   integer f;

   initial begin // head
      $display("time  syn_clr/load/en/up  q\n");
      f = $fopen("output.log","w");
      $fmonitor(f,"%5d,  %b%b%d%d  %d%d  %s",
               $time, reset, load, en_old, up_old, q,gold, err_msg);
          #1500  
            $fclose(f);     
          $stop;               // stop simulation
   end

   always @(posedge clk) begin
      // _old: the value sampled at the previous clock edge
      syn_clr_old <= reset;
//      en_old <= en;
      load_old <= load;
//      up_old <= up;
      q_old <= q;
      d_old <= d;

      // calculate the desired "gold" value
      if (syn_clr_old) begin
         gold = 0;
         up_old = 0;
         down_old = 0;
         en_old = 0;
         den_old = 0;
         dec_old = 0;
      end else if (up_old == 3) begin 
         en_old = 1;
      end
       else if (load_old) begin
         up_old = up_old + 1;
      end
      else if (dec) begin 
        dec_old = dec_old + 1;
      end else if (dec_old==3) begin
                den_old = 1; 
      end
      
       if (en_old) begin
         gold = gold + 1;
         en_old = 0;
         up_old = 0;
      end else if (den_old) begin 
      gold = gold - 1;
         den_old = 0;
         dec_old = 0;
//         up_old = 0;
         end
      // error message
      if (q==gold) begin
         err_msg = "     ";  // result passes
      end else if (q!=gold && load_old==1) begin
         err_msg = "ERROR";  // result fails
      end
      //
      $display("%5d,  %b%b%d%d  %d%d  %s",
               $time, reset, load, en_old, up_old, q,gold, err_msg);
           
           
   end
endmodule