`include "timescale.v"
module tb_insideFSM;

reg              clock         ;
reg              reset         ;
reg     [1:0]    io_sensor     ;
reg              io_clear      ;
reg              clock         ;
reg              reset         ;
reg     [1:0]    io_sensor     ;
reg              io_clear      ;
reg              clock         ;
reg              reset         ;
reg              io_enter      ;
reg              io_exit       ;
reg              io_clear      ;
wire    [6:0]    io_LED_out    ;

insideFSM uut (
    .clock         (    clock         ),
    .reset         (    reset         ),
    .io_sensor     (    io_sensor     ),
    .io_clear      (    io_clear      ),
    .clock         (    clock         ),
    .reset         (    reset         ),
    .io_sensor     (    io_sensor     ),
    .io_clear      (    io_clear      ),
    .clock         (    clock         ),
    .reset         (    reset         ),
    .io_enter      (    io_enter      ),
    .io_exit       (    io_exit       ),
    .io_clear      (    io_clear      ),
    .io_LED_out    (    io_LED_out    )
);

parameter PERIOD = 10;

initial begin
    $dumpfile("db_tb_insideFSM.vcd");
    $dumpvars(0, tb_insideFSM);
    clock = 1'b0;
    #(PERIOD/2);
    forever
        #(PERIOD/2) clock = ~clock;
end

endmodule
