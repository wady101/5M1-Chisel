# 5M1 Repo


This is my work done for the course module 5M1-Integrated Systems Design taught in Trinity. Effort has been made to write all the lab assignments in Chisel (and Matlab). 

My final verdict on using in Chisel - like any other HLS language, one needs to know what every line of the code will be synthesized to, else you will be in thick shit. 

# Repo Organization 


* _lab1_ : Simple FSM design of a Car-park with testbench 
* _lab2_ : Audio filter design in Matlab
* _lab3_ : Filter design (with coeffients provided in a csv) again with a (semi)-working testbench
* _lab4_ : ALU RISC processor design which features flexible number of registers. Synthesizable to bitstream - however LCD in Basys3 do not reflect output at times. Further debugging needed - but required testbenches made and added. Instructions supported - Basic Arithmetic, Basic memory management commands. Page Table is a feature to be added. 


All of this is under MIT License - Please cite this if using.


<object data="https://drive.google.com/file/d/1roW6SI1sTJtw7ZvllJOoFypuWlYmjeVz/view?usp=sharing" type="application/pdf" width="700px" height="700px">
    <embed src="https://drive.google.com/file/d/1roW6SI1sTJtw7ZvllJOoFypuWlYmjeVz/view?usp=sharing">
        <p>This browser does not support PDFs. Please download the PDF to view it: <a href="https://drive.google.com/file/d/1roW6SI1sTJtw7ZvllJOoFypuWlYmjeVz/view?usp=sharing">Download PDF</a>.</p>
    </embed>
</object>



