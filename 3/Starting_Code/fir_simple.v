`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.02.2020 23:57:10
// Design Name: 
// Module Name: fir_simple
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fir_simple(
  input                    clk,
  input                    reset,
  input      signed [7:0]  Xin,
  output reg signed [15:0] Yout
  );
  
  //Internal variables.
  wire signed   [7:0] H0,H1,H2,H3;
  wire signed   [15:0] M0,M1,M2,M3,add_out1,add_out2,add_out3;
  reg signed    [15:0] Q1,Q2,Q3;
    
//Filter coefficient initializations.
//H = [-2 -1 3 4].
  assign H0 = -'d2;
  assign H1 = -'d1;
  assign H2 =  'd3;
  assign H3 =  'd4;

//Multiple constant multiplications.
  assign M3 = H3*Xin;
  assign M2 = H2*Xin;
  assign M1 = H1*Xin;
  assign M0 = H0*Xin;

//Adders
  assign add_out1 = Q1 + M2;
  assign add_out2 = Q2 + M1;
  assign add_out3 = Q3 + M0;    


//Assign the last adder output to final output.
//Flipflop instantiations (for introducing a delay).
  always@ (posedge clk) begin
    if (reset) begin
      Q1   <= 'd0;
      Q2   <= 'd0;
      Q3   <= 'd0;
      Yout <= 'd0;
    end else begin
      Q1   <= M3;
      Q2   <= add_out1;
      Q3   <= add_out2;
      Yout <= add_out3;
    end
  end

endmodule
