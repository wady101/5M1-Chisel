`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.02.2020 00:01:25
// Design Name: 
// Module Name: fir_simple_direct_form
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fir_simple_direct_form(
  input                    clk,
  input                    reset,
  input      signed [7:0]  Xin,
  output reg signed [15:0] Yout
  );
  
//Internal variables.
  wire signed   [7:0] H0,H1,H2,H3;
  wire signed   [15:0] M0,M1,M2,M3;
  wire signed   [15:0] add_out1,add_out2,add_out3;
  reg signed    [7:0] Q1,Q2,Q3;
    
//Filter coefficient initializations.
//H = [-4 -2 6 8].
  assign H0 = -'d4;
  assign H1 = -'d2;
  assign H2 =  'd6;
  assign H3 =  'd8;

//Multiple constant multiplications.
  assign M0 = H0*Xin;
  assign M1 = H1*Q1;
  assign M2 = H2*Q2;
  assign M3 = H3*Q3;

//Adders
  assign add_out1 = M0 + M1;
  assign add_out2 = add_out1 + M2;
  assign add_out3 = add_out2 + M3;

//Assign the last adder output to final output.
//Flipflop instantiations (for introducing a delay).
  always@ (posedge clk) begin
    if (reset) begin
      Q1   <= 'd0;
      Q2   <= 'd0;
      Q3   <= 'd0;
      Yout <= 'd0;
    end else begin
      Q1   <= Xin;
      Q2   <= Q1;
      Q3   <= Q2;
      Yout <= add_out3;
    end
  end

endmodule
