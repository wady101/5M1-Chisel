`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.02.2020 00:14:40
// Design Name: 
// Module Name: tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb;

    // Inputs
    reg clk;
    reg signed [7:0] Xin;

    // Outputs
    wire signed [15:0] Yout;
    
    // File Handles
    integer logfile;

    // Instantiate the Unit Under Test (UUT)
    fir_simple_direct_form_pp uut (
        .clk(clk), 
        .Xin(Xin), 
        .Yout(Yout)
    );
    
    //Generate a clock with 10 ns clock period.
    initial begin
        clk = 0;
        forever #5 clk =~clk;
    end
    
    initial begin
        logfile = $fopen("filt_out.csv", "w");
        if (!logfile) $display("ERROR: COULD NOT OPEN LOGFILE");
        $fdisplay(logfile,"Time, Xin, Yout,");
        forever #10 $fdisplay(logfile,"%d ns, %d, %d,",$time, Xin, Yout);
    end
        
    

//Initialize and apply the inputs.
    initial begin
        Xin =  'd0;  #40;
        Xin = 'd1; #10;
        Xin = 'd0; #80;
        Xin = -'d3;  #10;
        Xin =  'd1;  #10;
        Xin =  'd0;  #10;
        Xin = -'d2;  #10;
        Xin = -'d1;  #10;
        Xin =  'd4;  #10;
        Xin = -'d5;  #10;
        Xin =  'd6;  #10;
        Xin =  'd0;  #10;
        #30;
        $fclose(logfile);
        $finish;
    end
      
endmodule
