// See README.md for license details.


import chisel3._
import chisel3.util._
import chisel3.experimental._ 
import scala.math._

// testing 

import chisel3.iotesters._
import org.scalatest.{Matchers, FlatSpec}
class FIR (val rows:Array[Int], val count:Int ) extends Module {
  val io = IO(new Bundle {
    val X        = Input(Vec(count,SInt(16.W)))
    val Y_out     = Output(SInt(64.W))
  })

   val sum  = Reg(SInt(64.W))
  val temp = Reg(Vec(count,SInt(64.W)))

  var sumVar = 0.S(64.W)
  for(ct <- 0 until count ) {
   temp(ct) := new  fromIntToLiteral(rows(ct): Int).S
   sumVar = sumVar + temp(ct) * io.X(ct)
  }
  sum := sumVar    // Connect the finished logic to the sum's "next" value.
  io.Y_out := sum  // Connect current value of sum register to output
  
}

object FIRDriver extends App {
  var nrows = 35
  println(s"CSV file reading begins (Can't be greater than $nrows)")
  val bufferedSource = io.Source.fromFile("/home/wadhwae/MAI/5M1IntegratedDesign/3/Starting_Code/filter_taps_integer.csv")
  // val rows = Array.ofDim[String](nrows)
  var count = 0
  // for (line <- bufferedSource.getLines) {
    // println(line.split(",").map(_.trim).toString) https://users.scala-lang.org/t/what-is-a-ljava-lang-string-and-how-to-print-its-content-human-readably/1088/2
    
    val rows = bufferedSource.getLines.map(_.split(",").head.trim.toFloat.toInt).toArray
    
    // count += 1
  // }
  // bufferedSource.close


  // DEBUG println(rows.mkString(" ")) // can be accessed by row(count)
  println(s"CSV file read with $count FIR variables")
  chisel3.Driver.execute(args, () => new FIR(rows,nrows))
}
