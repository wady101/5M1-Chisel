

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}
import scala.math.{pow, sin, Pi}
import java.io.File
import java.io.PrintWriter


class FIRUnitTester(c: FIR, val rows:Array[Int], val count:Int ) extends PeekPokeTester(c) {

    // test data
    val n = 35
    val bitwidth = 10
    val sine_freq = 10
    val samp_freq = 100
    val amp = 0.5

    // sample data, scale to between 0 and 2^bitwidth
    val max_value = pow(2, bitwidth)-1
    val sine = (0 until n).map(i => (max_value/2 + max_value/2*amp*sin(2*Pi*sine_freq/samp_freq*i)).toInt)

    // Lack of time  so no implementation - but golden vectors generator 
    // val expected = filter(DenseVector(sine.toArray), 
    //                       FIRKernel1D(DenseVector(coeffs.reverse.toArray), 1.0, ""), 
    //                       OptOverhang.None) 
    reset(5)
    val writer = new PrintWriter(new File("io.txt"))
    for (i <- 0 until n){
      poke(c.io.X(i), sine(i))
      println(s"cycle $i, got  ${peek(c.io.Y_out)}")
      writer.write(s"$i,${peek(c.io.Y_out)}\n")
      step(1)
    }
    writer.close()
    val sinewriter = new PrintWriter(new File("sine.txt"))

    for (i<-0 until n) {
      println(s"sinecycle $i, got ${sine(i)}")
      sinewriter.write(s"$i,${sine(i)}\n")
      step(1)
    }
    sinewriter.close()
    

}

/**
  * This is a trivial example of how to run this Specification
  * From within sbt use:
  * {{{
  * testOnly gcd.GCDTester
  * }}}
  * From a terminal shell use:
  * {{{
  * sbt 'testOnly gcd.GCDTester'
  * }}}
  */
class FIRTester extends ChiselFlatSpec {
  // Disable this until we fix isCommandAvailable to swallow stderr along with stdout
    var nrows = 35
  println(s"CSV file reading begins (Can't be greater than $nrows)")
  val bufferedSource = io.Source.fromFile("/home/wadhwae/MAI/5M1IntegratedDesign/3/Starting_Code/filter_taps_integer.csv")
  // val rows = Array.ofDim[String](nrows)
  var count = 0
  // for (line <- bufferedSource.getLines) {
    // println(line.split(",").map(_.trim).toString) https://users.scala-lang.org/t/what-is-a-ljava-lang-string-and-how-to-print-its-content-human-readably/1088/2
    
    val rows = bufferedSource.getLines.map(_.split(",").head.trim.toFloat.toInt).toArray
    
  private val backendNames = if(firrtl.FileUtils.isCommandAvailable(Seq("verilator", "--version"))) {
    Array("firrtl", "verilator")
  }
  else {
    Array("firrtl")
  }
  for ( backendName <- backendNames ) {
    "GCD" should s"calculate proper greatest common denominator (with $backendName)" in {
      Driver(() => new FIR(rows,nrows), backendName) {
        c => new FIRUnitTester(c,rows,nrows)
      } should be (true)
    }
  }

  "Basic test using Driver.execute" should "be used as an alternative way to run specification" in {
    iotesters.Driver.execute(Array(), () => new FIR(rows,nrows)) {
      c => new FIRUnitTester(c,rows,nrows)
    } should be (true)
  }

  if(backendNames.contains("verilator")) {
    "using --backend-name verilator" should "be an alternative way to run using verilator" in {
      iotesters.Driver.execute(Array("--backend-name", "verilator"), () => new FIR(rows,nrows)) {
        c => new FIRUnitTester(c,rows,nrows)
      } should be(true)
    }
  }

  "running with --is-verbose" should "show more about what's going on in your tester" in {
    iotesters.Driver.execute(Array("--is-verbose"), () => new FIR(rows,nrows)) {
      c => new FIRUnitTester(c,rows,nrows)
    } should be(true)
  }

  /**
    * By default verilator backend produces vcd file, and firrtl and treadle backends do not.
    * Following examples show you how to turn on vcd for firrtl and treadle and how to turn it off for verilator
    */

  "running with --generate-vcd-output on" should "create a vcd file from your test" in {
    iotesters.Driver.execute(
      Array("--generate-vcd-output", "on", "--target-dir", "test_run_dir/make_a_vcd", "--top-name", "make_a_vcd"),
      () => new FIR(rows,nrows)
    ) {

      c => new FIRUnitTester(c,rows,nrows)
    } should be(true)

    // new File("test_run_dir/make_a_vcd/make_a_vcd.vcd").exists should be (true)
  }

  "running with --generate-vcd-output off" should "not create a vcd file from your test" in {
    iotesters.Driver.execute(
      Array("--generate-vcd-output", "off", "--target-dir", "test_run_dir/make_no_vcd", "--top-name", "make_no_vcd",
      "--backend-name", "verilator"),
      () => new FIR(rows,nrows)
    ) {

      c => new FIRUnitTester(c,rows,nrows)
    } should be(true)

    // new File("test_run_dir/make_no_vcd/make_a_vcd.vcd").exists should be (false)

  }

}
