// See README.md for license details.


import chisel3._

/**
  * This provides an alternate way to run tests, by executing then as a main
  * From sbt (Note: the test: prefix is because this main is under the test package hierarchy):
  * {{{
  * test:runMain gcd.GCDMain
  * }}}
  * To see all command line options use:
  * {{{
  * test:runMain gcd.GCDMain --help
  * }}}
  * To run with verilator:
  * {{{
  * test:runMain gcd.GCDMain --backend-name verilator
  * }}}
  * To run with verilator from your terminal shell use:
  * {{{
  * sbt 'test:runMain gcd.GCDMain --backend-name verilator'
  * }}}
  */
object FIRMain extends App {
    var nrows = 35
  println(s"CSV file reading begins (Can't be greater than $nrows)")
  val bufferedSource = io.Source.fromFile("/home/wadhwae/MAI/5M1IntegratedDesign/3/Starting_Code/filter_taps_integer.csv")
  // val rows = Array.ofDim[String](nrows)
  var count = 0
  // for (line <- bufferedSource.getLines) {
    // println(line.split(",").map(_.trim).toString) https://users.scala-lang.org/t/what-is-a-ljava-lang-string-and-how-to-print-its-content-human-readably/1088/2
    
    val rows = bufferedSource.getLines.map(_.split(",").head.trim.toFloat.toInt).toArray
    
    // count += 1
  // }
  // bufferedSource.close


  iotesters.Driver.execute(args, () => new FIR(rows,nrows)) {
    c => new FIRUnitTester(c,rows,nrows)
  }
}

