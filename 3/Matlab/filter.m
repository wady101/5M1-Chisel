Fs=441;
f_sine=0.1*Fs;
amp_sine=0.5;
ts=1/Fs;
T=5;
t=0:ts:T;
x= amp_sine * sin(2*pi*f_sine*t);
plot(t,x);

Numerator = csvread('../Starting_Code/filter_taps_integer.csv');
Numerator = Numerator(:,1);
B = 2; % Number of bits
L = floor(log2((2^(B-1)-1)/max(Numerator)));  % Round towards zero to avoid overflow
bq = Numerator*2^L;
scale =  1/max(abs(bq));
bq=bq*scale;
% bq = round(bq,3,'significant')
% bq = round(bq,2);

% bq = fi(Numerator, true, B);  % signed = true, B = 18 bits
% L = bq.FractionLength;
Hd = dfilt.dffir(bq);
% Hd.Arithmetic = 'fixed';
% Hd.CoeffWordLength = B;
% Hd.InputWordLength = 8;
Hd.Arithmetic = 'fixed';
% Hd.CoeffAutoScale = false;
% Hd.NumFracLength = 1;
% Hd.InputWordLength = 2;
% Hd.InputFracLength = 2;
% Hd.CoeffWordLength = 3;
% Hd.InputWordLength = 8;
y = filter(Hd,x); %full precision output
% freqz(double(Hd))

% Double precision output
Hdouble=double(Hd);%dfilt.dffir(Numerator);
yd = double(y);%filter(Hdouble,x);
norm(yd-double(y))

xscext = double(x);
gd = grpdelay(Hd);
yidx = idx + gd(1);
yscext = double(y);
hold on
p1=plot(t, xscext);
p2=plot(t, yscext);
h = [p1(1);p2];
% axis([4 5 -2.5e8 2.5e8]);
legend(h,'input', 'output');
set(gcf, 'color', 'white');
hold off

% % Hd.FilterInternals = 'SpecifyPrecision';
% specifyall(Hd);
% Hd.OutputWordLength = 16;
% % Hd.OutputFracLength = 9;
% y2 = filter(Hd, x);
% norm(double(y) - double(y2))

% plot(t,y2);
intreader=all(Hd.Numerator == round(Hd.Numerator))
