[x, Fs] = audioread('speech_19.wav'); % audioread in Matlab versions >2014
nfft= 2^10;
X= fft(x, nfft);
fstep = Fs/nfft;
fvec= fstep*(0: nfft/2-1);
[pk,idx]= max(2*abs(X(1:nfft/2))); % gets me fft and indicator
%pk_f=idx*fstep; % returns me Fs/nfft*nfft = Fs. 
pk_f = 0.24*Fs
disp(pk_f)
disp(pk)
 figure1 = figure('visible','off')
%plot(fvec,2*abs(X(1:nfft/2)))
%title('Single-Sided Amplitude Spectrum of x(t)')
%xlabel('Frequency (Hz)') %468
%ylabel('|X(f)|')
%saveas(figure1,'figure1.jpg')

Fpass1 = pk_f-(0.5*Fs);             % First Passband Frequency
Fstop1 = pk_f-(0.4*Fs);             % First Stopband Frequency
Fstop2 = pk_f+(0.4*Fs);             % Second Stopband Frequency
Fpass2 = pk_f+(0.5*Fs);             % Second Passband Frequency
Dpass1 = 0.028774368332;  % First Passband Ripple
Dstop  = 0.011350108157;  % Stopband Attenuation
Dpass2 = 0.028774368332;  % Second Passband Ripple
dens   = 20;              % Density Factor


[N, Fo, Ao, W] = firpmord([Fpass1 Fstop1 Fstop2 Fpass2]/(Fs/2), [1 0 ...
                          1], [Dpass1 Dstop Dpass2]);
txt = sprintf('N-taps = %d \n Fpass1 = %fHz \n Fpass2 = %fHz \n Fstop1 = %fHz \n  Fstop2 = %fHz ', N, Fpass1, Fpass2, Fstop1, Fstop2)

% Calculate the coefficients using the FIRPM function.
b  = firpm(N, Fo, Ao, W, {dens});
Hd = dfilt.dffir(b);
%figure3=figure(5)
%freqz(Hd)
%saveas(figure3,'figure5.jpg')
%freqz(Hd)

y = filter(Hd,x);
% sound(y,Fs)
Y= fft(y, nfft);
fstep = Fs/nfft;
fvec= fstep*(0: nfft/2-1);
[pk,idx]= max(2*abs(Y(1:nfft/2))); % gets me fft and indicator
pk_f=idx*fstep; % returns me Fs/nfft*nfft = Fs. 
audiowrite('filtered.wav',y,Fs);
lsb = 2^-7;% least significant bit we can hold
int_part = b ./lsb;% how many lsbs in each coefficient
int_part = round(int_part);% round for accuracy
b_q = int_part .* lsb;% quantises the entire vector with no for loop!

Hd_q= dfilt.dffir(b_q);% the quantised versio
y = filter(Hd_q,x);
audiowrite('quantized.wav',y,Fs);
%freqz(Hd_q)

% X= fft(y, nfft);
% fstep = Fs/nfft;
% fvec= fstep*(0: nfft/2-1);
% [pk,idx]= max(2*abs(X(1:nfft/2))); % gets me fft and indicator
% pk_f=idx*fstep; % returns me Fs/nfft*nfft = Fs. 
% plot(fvec,2*abs(X(1:nfft/2)))
% title('Single-Sided Amplitude Spectrum of x(t)')
% xlabel('Frequency (Hz)') %468
% ylabel('|X(f)|')
% saveas(figure1,'figure1.jpg')
%b=typecast(b,'uint16');
b_q=b_q.*  10^2;
res = sum(sum( dec2bin(abs(b_q)).' == '1' ));
adders = N * res; 
txt = sprintf('Number fo adders %d', adders)
%figure2=figure('visible','off')
%plot(fvec,2*abs(Y(1:nfft/2)));
%title('Single-Sided Amplitude Spectrum of y(t) N=654')
%xlabel('Frequency (Hz)') %468
%ylabel('|Y(f)|')
%saveas(figure2,'figure2.jpg')
